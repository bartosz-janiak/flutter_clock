import 'dart:math';
import 'package:flare_flutter/flare.dart';
import 'package:flare_dart/math/mat2d.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';

typedef void OnUpdated();

class HouseController extends FlareController {
  static const double DemoMixSpeed = 10;
  static const double FPS = 60;

  final OnUpdated demoUpdated;

  HouseController({this.demoUpdated});

  int _rooms = 8;
  FlutterActorArtboard _artboard;
  FlareAnimationLayer _skyAnimation;

  final List<FlareAnimationLayer> _roomAnimations = [];

  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    /// Advance the background animation every frame.
    _skyAnimation.time =
        (_skyAnimation.time + elapsed) % _skyAnimation.duration;
    _skyAnimation.apply(artboard);

    /// Iterate from the top b/c elements might be removed.
    int len = _roomAnimations.length - 1;
    for (int i = len; i >= 0; i--) {
      FlareAnimationLayer layer = _roomAnimations[i];
      layer.time += elapsed;

      /// The mix quickly ramps up to 1, but interpolates for the first few frames.
      layer.mix = min(1.0, layer.time / 0.07);
      layer.apply(artboard);

      /// When done, remove it.
      if (layer.isDone) {
        _roomAnimations.removeAt(i);
      }
    }

    /// If the app is still in demo mode, the mix is positive
    /// Otherwise quickly ramp it down to stop the animation.

    return true;
  }

  /// Grab the references to the right animations, and
  /// packs them into [FlareAnimationLayer] objects
  @override
  void initialize(FlutterActorArtboard artboard) {
    _artboard = artboard;
    _skyAnimation = FlareAnimationLayer()
      ..animation = _artboard.getAnimation("wind")
      ..mix = 1.0;
    ActorAnimation endAnimation = artboard.getAnimation("to 6");
    if (endAnimation != null) {
      endAnimation.apply(endAnimation.duration, artboard, 1.0);
    }
  }

  @override
  void setViewTransform(Mat2D viewTransform) {}

  /// Use the [demoUpdated] callback to relay the current number of rooms
  /// to the [Page] widget, so it can position the slider accordingly.

  _enqueueAnimation(String name) {
    ActorAnimation animation = _artboard.getAnimation(name);
    if (animation != null) {
      _roomAnimations.add(FlareAnimationLayer()..animation = animation);
    }
  }

  set rooms(int value) {
    if (_rooms == value) {
      return;
    }

    print(value);

    /// Sanity check.
    if (_artboard != null) {
      /// Add the animation with room [value] to the list.
      _enqueueAnimation("to $value");
    }
    _rooms = value;
  }

  int get rooms => _rooms;
}
