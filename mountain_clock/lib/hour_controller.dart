import 'dart:math';
import 'package:flare_flutter/flare.dart';
import 'package:flare_dart/math/mat2d.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';

typedef void OnUpdated();

class HourController extends FlareController {
  static const double DemoMixSpeed = 10;
  static const double FPS = 60;

  HourController(int startHour) {
    this._hour = startHour;
  }

  int _hour;

  FlutterActorArtboard _artboard;
  FlareAnimationLayer _skyAnimation;

  final List<FlareAnimationLayer> _roomAnimations = [];

  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    _skyAnimation.time =
        (_skyAnimation.time + elapsed) % _skyAnimation.duration;
    _skyAnimation.apply(artboard);

    int len = _roomAnimations.length - 1;
    for (int i = len; i >= 0; i--) {
      FlareAnimationLayer layer = _roomAnimations[i];
      layer.time += elapsed;

      layer.mix = min(1.0, layer.time / 0.07);
      layer.apply(artboard);

      if (layer.isDone) {
        _roomAnimations.removeAt(i);
      }
    }

    return true;
  }

  @override
  void initialize(FlutterActorArtboard artboard) {
    _artboard = artboard;
    _skyAnimation = FlareAnimationLayer()
      ..animation = _artboard.getAnimation("wind")
      ..mix = 1.0;
    ActorAnimation endAnimation = artboard.getAnimation("to $hour");
    if (endAnimation != null) {
      endAnimation.apply(endAnimation.duration, artboard, 1.0);
    }
  }

  @override
  void setViewTransform(Mat2D viewTransform) {}

  _enqueueAnimation(String name) {
    ActorAnimation animation = _artboard.getAnimation(name);
    if (animation != null) {
      _roomAnimations.add(FlareAnimationLayer()..animation = animation);
    }

    // demoUpdated();
  }

  set hour(int value) {
    if (_hour == value) {
      return;
    }

    if (_artboard != null) {
      _hour = value;
      _enqueueAnimation("to $_hour");
    }
  }

  int get hour => _hour;
}
