import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Minutes extends StatefulWidget {
  final int minute;

  Minutes({this.minute, Key key}) : super(key: key);

  @override
  _MinutesState createState() => _MinutesState();
}

class _MinutesState extends State<Minutes> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Row(children: [
        ...List.generate(widget.minute, (index) {
          return Text((index).toString(),
              style: TextStyle(
                color: Colors.white.withOpacity(0.2),
                fontFamily: "GrandHotel",
              ));
        }),
        Text(
          widget.minute.toString(),
          style: DefaultTextStyle.of(context).style.apply(
              fontSizeFactor: 2.0,
              fontFamily: "GrandHotel",
              color: Colors.white.withOpacity(0.8)),
        ),
        ...List.generate(60 - widget.minute, (index) {
          return Text((index + widget.minute + 1).toString(),
              style: TextStyle(
                color: Colors.white.withOpacity(0.2),
                fontFamily: "GrandHotel",
              ));
        }),
      ]),
    );
  }
}
