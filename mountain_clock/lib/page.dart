import 'dart:async';

import 'package:MountainClock/minutes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flare_flutter/flare_actor.dart';

import 'hour_controller.dart';

class Page extends StatefulWidget {
  final String title;

  Page({this.title, Key key}) : super(key: key);

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<Page> with SingleTickerProviderStateMixin {
  HourController _hourController;

  var _now = DateTime.now();
  Timer _timer;

  @override
  void initState() {
    _hourController = HourController(_now.hour);

    _updateTime();

    super.initState();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  void _updateTime() {
    setState(() {
      _now = DateTime.now();
      _hourController.hour = _now.hour;

      _timer = Timer(
        Duration(seconds: 1) - Duration(milliseconds: _now.millisecond),
        _updateTime,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Listener(
      child: Stack(fit: StackFit.expand, children: [
        FlareActor(
          "assets/Clock.flr",
          controller: _hourController,
          fit: BoxFit.cover,
        ),
        Container(
            margin: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Minutes(
                  minute: _now.minute,
                )
              ],
            ))
      ]),
    )));
  }
}
